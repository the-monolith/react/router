import { React } from 'local/react/component'

type Children = React.ReactNode

type IRoute = () => Children

export type IRoutes = {
  [key: string]: IRoute
}

export const navigate = (to: string) => <T extends HTMLElement>(
  event: React.MouseEvent<T>
) => {
  event.preventDefault()
  history.pushState({}, '', to)
  stateChangeListeners.forEach(listener => listener())
}

const stateChangeListeners: Array<() => void> = []

export class Router extends React.Component<{ routes: IRoutes }> {
  onPopState: () => void

  componentDidMount() {
    this.onPopState = () => {
      this.forceUpdate()
    }

    window.addEventListener('popstate', this.onPopState)
    stateChangeListeners.push(this.onPopState)
  }

  componentWillUnmount() {
    window.removeEventListener('popstate', this.onPopState)
    stateChangeListeners.splice(
      stateChangeListeners.indexOf(this.onPopState),
      1
    )
  }

  getMatchingRoute(location: string) {
    const { routes } = this.props

    let route: IRoute

    Object.keys(routes).some(matchUrl => {
      if (location === matchUrl) {
        route = routes[matchUrl]

        return true
      }
    })

    if (typeof route === 'undefined') {
      route = () => this.notFound()
    }

    return route
  }

  notFound() {
    return <h1>Error: Page not found</h1>
  }

  render() {
    const matchingRoute = this.getMatchingRoute(window.location.pathname)

    return matchingRoute()
  }
}
